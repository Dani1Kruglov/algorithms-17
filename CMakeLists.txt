cmake_minimum_required(VERSION 3.21)
project(Alg17)

set(CMAKE_CXX_STANDARD 14)

add_executable(Alg17 main.cpp)
